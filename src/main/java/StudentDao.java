import java.sql.*;

/**
 * Created by sec on 2015-12-26.
 */
public class StudentDao {

    public void add(Student student) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection c = DriverManager.getConnection(
                "jdbc:mysql://localhost/db1", "root", "123456");

        PreparedStatement ps = c.prepareStatement(
                "insert into student(name, age, meal_a_Day) value(?,?,?)");
        ps.setString(1, student.getName());
        ps.setInt(2, student.getAge());
        ps.setInt(3, student.getMeal_a_day());

        ps.executeUpdate();

        ps.close();
        c.close();

    }

    public Student get(String name) throws ClassNotFoundException,SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection c = DriverManager.getConnection(
                "jdbc:mysql://localhost/db1", "root", "123456");

        PreparedStatement ps = c.prepareStatement(
                "select * from student where name = ?");
        ps.setString(1, name);

        ResultSet rs = ps.executeQuery();
        rs.next();
        Student student = new Student();
        student.setName(rs.getString("name"));
        student.setAge(rs.getInt("age"));
        student.setMeal_a_day(rs.getInt("meal_a_Day"));

        rs.close();
        ps.close();
        c.close();

        return student;

    }
}
