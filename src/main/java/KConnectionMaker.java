import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by sec on 2016-01-02.
 */
public class KConnectionMaker implements ConnectionMaker {

    public Connection makeNewConnection() throws ClassCastException, SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection c = DriverManager.getConnection(
                "jdbc:mysql://localhost/krkdb", "root", "123456");

        return c;
    }

}
