import java.sql.SQLException;

/**
 * Created by sec on 2015-12-26.
 */
public class Main {

    public static void main(String[] args) throws  ClassNotFoundException, SQLException{

        // 드래그 + ctrl shift /

        UserDao dao = new UserDao();

        User user = new User();
        user.setId("whiteship");
        user.setName("백기선");
        user.setPassword("married");

        dao.add(user);

        System.out.println(user.getId() + "등록 성공");

        User user2 = dao.get(user.getId());
        System.out.println(user2.getName());
        System.out.println(user2.getPassword());

        /*
        StudentDao sdao = new StudentDao();
        Student student1 = new Student("가나다", 27, 3);
        sdao.add(student1);

        System.out.println(student1.getName() + "등록 성공");


        Student student2 = sdao.get("강은지");
        System.out.println(student2.getName());
        System.out.println(student2.getAge());
        System.out.println(student2.getMeal_a_day());
*/

    }



}
