/**
 * Created by sec on 2015-12-26.
 */
public class Student {
    String name;
    int age;
    int meal_a_day;

    public Student() {
    }

    public Student(String name, int age, int meal_a_day) {
        this.name = name;
        this.age = age;
        this.meal_a_day = meal_a_day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMeal_a_day() {
        return meal_a_day;
    }

    public void setMeal_a_day(int meal_a_day) {
        this.meal_a_day = meal_a_day;
    }
}
