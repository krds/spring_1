import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by sec on 2016-01-02.
 */
public interface ConnectionMaker {

    public Connection makeNewConnection() throws ClassCastException, SQLException, ClassNotFoundException;
}
